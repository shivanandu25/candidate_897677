##Get the code

Git:
git clone https://gitlab.com/shivanandu25/candidate_897677.git

## Serenity Cucumber BDD project
This project is to test the scenarios on the UK visa website to confirm whether a visa is required to visit UK.

### The project directory structure
The project has build scripts for Maven and with below directory structure:
```Gherkin
src
+ test
  + java                                Test runners and supporting code
    +++ CucumberTestSuite               To execute the tests
  + resources
    + features                          Feature files (check_uk_visa_website.feature)
    + webdriver                         Bundled WebDriver binaries      
```

### Dependencies used
Below are few of the included dependencies
JUnit, 
serenity-cucumber,
serenity-junit,
hamcrest-all,
Maven failsafe plugin,
Serenity Maven plugin

### Executing the tests
CucumberTestSuite test runner class can be used to run the project or by 'mvn verify' command.
By default, tests will run using chrome(Version 83).I have updated 'driver' = chrome in Serenity.conf file. This can be updated accordingly to change the webdriver.  
Below command can also be used to override the existing driver.
$ mvn clean verify -Ddriver=firefox

### Results
Results are recorded in target/site/serenity directory.

## Serenity Rest project
This project is to test the postcodes API and check the status code.

### The project directory structure
The project has build scripts for Maven and with below directory structure:
```Gherkin
src
  + test
    + java                                Test runner and supporting code
      +++ CucumberTestSuite                 To execute the tests
    + resources
      + features                          
        +status                            Feature files (search_postcode.feature)

```

### Dependencies used
Below are few of the included dependencies
JUnit,
serenity-cucumber4,
serenity-rest-assured,
hamcrest-all,
Maven failsafe plugin,
Serenity Maven plugin

### Executing the tests
CucumberTestSuite test runner class can be used to run the project or by 'mvn failsafe:integration-test failsafe:verify' command.
mvn serenity:aggregate' to generate aggregate file.

### Results
Results are recorded in target/site/serenity directory.

### Note: I have used IntelliJ IDEA for developing the project.

