package starter.status;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class SearchPostCodeStatus {
    private String POST_CODE_SEARCH ="https://api.postcodes.io/postcodes/";
    private Response response;

    @Step("I search for the postcode {}")
    public void searchPostCode(String searchPostCode) {
        response = SerenityRest.when().get(POST_CODE_SEARCH + searchPostCode);
    }

    @Step("search is executed successfully")
    public void searchIsExecutedSuccessfully(){
        response.then().statusCode(200);
    }

}
