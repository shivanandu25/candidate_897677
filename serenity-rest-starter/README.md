##Get the code

Git:
git clone https://gitlab.com/shivanandu25/candidate_897677.git

## Serenity Rest project
This project is to test the postcodes API and check the status code.

### The project directory structure
The project has build scripts for Maven and with below directory structure:
```Gherkin
src
  + test
    + java                                Test runner and supporting code
      +++ CucumberTestSuite                 To execute the tests
    + resources
      + features                          
        +status                            Feature files (search_postcode.feature)

```

### Dependencies used
Below are few of the included dependencies
JUnit,
serenity-cucumber4,
serenity-rest-assured,
serenity JUnit,
hamcrest-all,
Maven failsafe plugin,
Serenity Maven plugin

### Executing the tests
CucumberTestSuite test runner class can be used to run the project or by 'mvn failsafe:integration-test failsafe:verify' command.
mvn serenity:aggregate' to generate aggregate file.

### Results
Results are recorded in target/site/serenity directory.

