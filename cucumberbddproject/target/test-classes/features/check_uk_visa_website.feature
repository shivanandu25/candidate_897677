Feature: Check UK visa website for visa requirement

  @cucumber
  Scenario: Confirm whether a visa is required for Japan nationality with study reason
    Given I navigate to home office website
    And I provide a nationality of "Japan"
    And I select the reason "study" and submit form
    And I state I am intending to stay for more than 6 months
    When I submit the form
    Then I will be informed "You’ll need a visa to study in the UK"

  @cucumber
  Scenario: Confirm whether a visa is required for Russia nationality with tourism reason
    Given I navigate to home office website
    And I provide a nationality of "Russia"
    And I select the reason "tourism" and submit form
    And I state I am not travelling or visiting a partner or family
    When I submit the form
    Then I will be informed "You’ll need a visa to come to the UK"

  @cucumber
  Scenario: Confirm whether a visa is required for Japan nationality with tourism reason
    Given I navigate to home office website
    And I provide a nationality of "Japan"
    And I select the "tourism" reason
    When I submit the form
    Then I will be informed "You won’t need a visa to come to the UK"