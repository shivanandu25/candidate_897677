package net.serenitybdd.ukvisarequirement.search;

import org.openqa.selenium.By;

class SearchForm {
    static By NATIONALITY_DROPDOWN = By.id("response");
    static By MORE_THAN_SIX_MONTHS = By.cssSelector("input[value=longer_than_six_months]");
    static By NO_RADIO_BUTTON = By.cssSelector("input[value=no]");
}
