package net.serenitybdd.ukvisarequirement.search;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class ProvideNationality extends UIInteractionSteps {
SearchForm searchForm;
    @Step("I provide a nationality of {}")
    public void provideNationality(String nationality) {
        $(SearchForm.NATIONALITY_DROPDOWN).selectByVisibleText(nationality);
        $(SubmitForm.NEXT_STEP_BUTTON).click();
    }

    @Step("And I state I am not travelling or visiting a partner or family")
    public void clickOnNoButton() {
        $(SearchForm.NO_RADIO_BUTTON).click();
    }



}
