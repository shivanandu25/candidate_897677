package net.serenitybdd.ukvisarequirement.search;

import net.serenitybdd.core.steps.UIInteractionSteps;

public class VisaResult extends UIInteractionSteps {
    public String titleResult() {
        String resultText = $(ResultForm.RESULT_TITLE).getText();
        return resultText;

    }
}
