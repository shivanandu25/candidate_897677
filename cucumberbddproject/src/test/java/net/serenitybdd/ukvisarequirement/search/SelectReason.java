package net.serenitybdd.ukvisarequirement.search;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;

import static net.serenitybdd.ukvisarequirement.search.SubmitForm.NEXT_STEP_BUTTON;

public class SelectReason extends UIInteractionSteps {

    @Step("I select the reason {} and submit form")
    public void selectReasonAndSubmitForm(String reason) {
        find(By.cssSelector("input[value="+reason+"]")).click();
        $(NEXT_STEP_BUTTON).click();
    }

    @Step("I select the {} reason")
    public void selectReason(String reason) {
        find(By.cssSelector("input[value="+reason+"]")).click();
    }

    @Step("And I state I am intending to stay for more than 6 months")
    public void clickOnMoreThanSixMonths() {
        $(SearchForm.MORE_THAN_SIX_MONTHS).click();
    }

    @Step("I submit the form")
    public void clickOnNextStepButton() {
        $(NEXT_STEP_BUTTON).click();
    }

}
