package net.serenitybdd.ukvisarequirement.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.gov.uk/check-uk-visa/y")
class HomeOfficeHomePage extends PageObject {}
