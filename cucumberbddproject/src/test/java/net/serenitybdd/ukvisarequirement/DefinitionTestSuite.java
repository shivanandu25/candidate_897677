package net.serenitybdd.ukvisarequirement;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/check_uk_visa_website.feature")
public class DefinitionTestSuite {}
