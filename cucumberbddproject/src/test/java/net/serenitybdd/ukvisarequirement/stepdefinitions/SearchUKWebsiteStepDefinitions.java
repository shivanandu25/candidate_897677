package net.serenitybdd.ukvisarequirement.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.ukvisarequirement.navigation.NavigateTo;
import net.serenitybdd.ukvisarequirement.search.ProvideNationality;
import net.serenitybdd.ukvisarequirement.search.SelectReason;
import net.serenitybdd.ukvisarequirement.search.VisaResult;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

public class SearchUKWebsiteStepDefinitions {

    @Steps
    NavigateTo navigateTo;
    ProvideNationality searchFor;
    VisaResult visaResult;
    SelectReason selectReason;


    @Given("^I navigate to home office website$")
    public void i_am_on_the_home_office_home_page() {
        navigateTo.theHomeOfficeSitePage();
    }

    @When("^I provide a nationality of \"(.*)\"")
    public void i_provide_nationality(String nationality) {
        searchFor.provideNationality(nationality);
    }

    @When("^I select the \"(.*)\" reason")
    public void i_select_the_reason(String reason) {
        selectReason.selectReason(reason);
    }

    @When("^I select the reason \"(.*)\" and submit form")
    public void i_select_the_reason_submit_form(String reason) {
        selectReason.selectReasonAndSubmitForm(reason);
    }

    @When("^I state I am not travelling or visiting a partner or family")
    public void click_On_No_Radio_Button() {
        searchFor.clickOnNoButton();
    }

    @When("^I state I am intending to stay for more than 6 months")
    public void click_On_More_Than_6_Months() {
        selectReason.clickOnMoreThanSixMonths();
    }

    @When("^I submit the form")
    public void click_On_Next_Step_Button() {
        selectReason.clickOnNextStepButton();
    }

    @Then("I will be informed \"(.*)\"")
    public void result_If_I_Need_Visa(String expectedResult) {
        assertThat(visaResult.titleResult())
                .matches(expectedResult);
    }

}
